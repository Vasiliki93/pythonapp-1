
      (function () {

  'use strict';

  angular.module('CarApp', [])


            .config(['$interpolateProvider', function($interpolateProvider) {
                    $interpolateProvider.startSymbol('{a');
                    $interpolateProvider.endSymbol('a}');
            }])

          .controller('CarsController', ['$scope', '$log', '$http', function($scope, $log, $http) {


                        $http.get('http://127.0.0.1:5000/api/cars').
                          success(function(results) {
                            console.log(results);
                            $scope.apotelesmata = results
                          }).
                          error(function(error) {
                            $log.log(error);
                          });

          }
          ]);






}());
