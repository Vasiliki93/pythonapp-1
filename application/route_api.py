from application import app
from application import  api


#get all cars from database - vgoritsas
@app.route('/api/cars',methods=['GET'])
def get():
    ## apo to api pernei tin methodo displaycars
    return api.displayCars()

#insert data into database - vgoritsas
@app.route('/api/car/store', methods=['POST'])
def post():
    #apo to api pernei tin methods post
    return api.post()

## route by george michalopoulos
## sindesi me to api
@app.route('/api/search/<name>/<final_price>')
##pernei ta pedia name kai final_price
def search(name , final_price):
    return api.search(name, final_price)

