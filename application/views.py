#include app vairable from __init_.py
from  application import app
# include from flask render_template method to display html file.
from flask import render_template , url_for

# view cars admin area
@app.route('/admin/view/cars')
def view_cars():
    return render_template('admincars.html')

## create new car form - admin
@app.route('/admin/create')
def add_car():
    return  render_template('add_car.html')

## index view frond end
@app.route('/')
def index():
    return render_template('index.html')


